<?php

return [
    //driver could be either 'config' or 'database'
    'driver' => 'config',

    //define the table to use when the database driver is used. The default migration uses 'scopes'.
    'table' => 'scopes',

    //used when config is set as driver
    'scopes' => [
        [
            'id' => 1,
            'url' => env('APP_URL', 'http://localhost'),
            'name' => 'Main Website',
            'locales' => [
                'nl' => 'nl_NL',
            ],
            'default_locale' => 'nl',
        ],
    ],
];
