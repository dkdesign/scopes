<?php

namespace DekaWebdesign\Scopes\Tests\Unit;

use DekaWebdesign\Scopes\Scope;
use DekaWebdesign\Scopes\Tests\TestCase;

class ScopeTest extends TestCase
{

    /** @test */
    public function creating_a_scope_gives_it_a_default_name()
    {
        $scope = new Scope(collect(config('scopes'))->first());
        $this->assertEquals('Main Website', $scope->name);
    }

    /** @test */
    public function creating_a_scope_gives_it_a_default_locale()
    {
        $scope = new Scope(collect(config('scopes'))->first());
        $this->assertEquals('nl_NL', $scope->locale);
    }
}
