<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 25/08/2020
 * Time: 11:21
 */

namespace DekaWebdesign\Scopes;


use DekaWebdesign\Scopes\Exceptions\ScopeConfigException;
use Stringy\StaticStringy as SSy;

class Scope
{
    public $id;
    public $url;
    public $name;
    public $locales;
    public $locale_pfx;
    public $locale;
    public $shortLocale;

    public function __construct($data)
    {
        try {
            $this->url = $data['url'];
            $this->id = $data['id'];
            $this->name = $data['name'];
            $this->locales = collect($data['locales']);
            $this->setLocale($this->locales[$data['default_locale']], '');

        } catch (ScopeConfigException $e) {
            abort(500);
        }
    }

    public function absoluteUrl()
    {
        if (SSy::startsWith($url = $this->url, '/')) {
            $url = SSy::ensureLeft($url, request()->getSchemeAndHttpHost());
        }

        return SSy::removeRight($url, '/');
    }

    public function setLocale(string $locale, string $key)
    {
        $this->locale_pfx = $key;
        $this->locale = $locale;
        $this->shortLocale = explode('_', $locale)[0];
    }

}
