<?php

namespace DekaWebdesign\Scopes;

use DekaWebdesign\Scopes\Commands\ScopesCommand;
use DekaWebdesign\Scopes\Middleware\SetScope;
use DekaWebdesign\Scopes\Models\ScopeData;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ScopesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/scopes.php' => config_path('scopes.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/../resources/views' => base_path('resources/views/vendor/scopes'),
            ], 'views');

            $migrationFileName = 'create_scopes_table.php';
            if (! $this->migrationFileExists($migrationFileName)) {
                $this->publishes([
                    __DIR__ . "/../database/migrations/{$migrationFileName}.stub" => database_path('migrations/' . date('Y_m_d_His', time()) . '_' . $migrationFileName),
                ], 'migrations');
            }

            $this->commands([
                ScopesCommand::class,
            ]);
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/scopes.php', 'scopes');

        $this->app->singleton(Scopes::class, function () {
            if (config('scopes.driver') === 'config') {
                $scopedata = collect(config('scopes.scopes'));
            } else {
                $scopedata = ScopeData::all();
            }

            return new Scopes($scopedata);
        });

        $router = $this->app->make(Router::class);
        $router->pushMiddlewareToGroup('web', SetScope::class);
    }

    public static function migrationFileExists(string $migrationFileName): bool
    {
        $len = strlen($migrationFileName);
        foreach (glob(database_path("migrations/*.php")) as $filename) {
            if ((substr($filename, -$len) === $migrationFileName)) {
                return true;
            }
        }

        return false;
    }
}
