<?php

namespace DekaWebdesign\Scopes\Commands;

use Illuminate\Console\Command;

class ScopesCommand extends Command
{
    public $signature = 'Scopes';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
