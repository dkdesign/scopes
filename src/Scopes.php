<?php

namespace DekaWebdesign\Scopes;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Stringy\StaticStringy as SSy;

class Scopes
{
    protected $scopes;
    protected $current;

    public function __construct(Collection $scopes)
    {
        $this->scopes = $scopes->map(function ($scope) {
            return new Scope($scope);
        });
    }

    public function default()
    {
        return $this->scopes->first();
    }

    public function all()
    {
        return $this->scopes;
    }

    public function hasMultiple()
    {
        return $this->scopes->count() > 1;
    }

    public function findByUrl($url)
    {
        $url = Str::before($url, '?');
        $url = SSy::ensureRight($url, '/');

        return collect($this->scopes)->filter(function ($scope) use ($url) {
            return SSy::startsWith($url, SSy::ensureRight($scope->absoluteUrl(), '/'));
        })->first();
    }

    public function setCurrent()
    {
        $uri = request()->getHttpHost();
        $this->current = collect($this->scopes)->filter(function ($scope) use ($uri) {
            return SSy::startsWith($uri, SSy::ensureRight($scope->absoluteUrl(), '/'));
        })->first();
    }

    public function getCurrent()
    {
        return $this->current
            ?? $this->findByUrl(request()->getUri())
            ?? $this->default();
    }
}
