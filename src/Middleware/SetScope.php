<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 25/08/2020
 * Time: 19:05
 */

namespace DekaWebdesign\Scopes\Middleware;

use Closure;
use DekaWebdesign\Scopes\Scopes;
use Illuminate\Support\Arr;

class SetScope
{
    public function handle($request, Closure $next)
    {
        $scopes = app()->make(Scopes::class);
        $scopes->setCurrent();
        $current = $scopes->getCurrent();
        $this->setLocale($current, $request);

        return $next($request);
    }

    protected function setLocale($scope, $request)
    {
        if ($scope->locales->count() > 1 && count($request->segments())) {
            $lang_url = $request->segments()[0];
            if (Arr::exists($scope->locales, $lang_url)) {
                $key = ((string) $scope->locales->first() === $lang_url)
                    ? ''
                    : $lang_url;
                $scope->setLocale($scope->locales[$lang_url], $key);
            }
        }

        setlocale(LC_TIME, $scope->locale);
        app()->setLocale($scope->shortLocale);
    }
}
