<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 25/08/2020
 * Time: 11:30
 */

namespace DekaWebdesign\Scopes\Exceptions;

use Exception;

class ScopeConfigException extends Exception
{

}
