<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 10/09/2020
 * Time: 09:46
 */

namespace DekaWebdesign\Scopes\Models;


class ScopeData extends \Illuminate\Database\Eloquent\Model
{
    protected $guarded = [];
    protected $casts = ['locales' => 'array'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(config('scopes.table', 'scopes'));
    }

    public static function getBySlug($slug)
    {
        return self::where('slug', $slug)->first();
    }

    public static function getCursor()
    {
        return self::pluck('name', 'id')->toArray();
    }

}
