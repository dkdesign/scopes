<?php

namespace DekaWebdesign\Scopes;

use Illuminate\Support\Facades\Facade;

/**
 * @see \DekaWebdesign\Scopes\Scopes
 */
class ScopesFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Scopes';
    }

    public static function getCurrent()
    {
        $scopes = app()->make(Scopes::class);
        return $scopes->getCurrent();
    }

    public static function getLocalePrefix()
    {
        $scopes = app()->make(Scopes::class);
        return $scopes->getCurrent()->locale_pfx;
    }

    public static function getAll()
    {
        $scopes = app()->make(Scopes::class);
        return $scopes->all();
    }

    public static function getCurrentLocaleLong()
    {
        $current = self::getCurrent();
        return collect($current->locales)->first();
    }
}
