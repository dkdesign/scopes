# This package adds configurable scoping to a laravel application based on the url through which it is loaded.

[![Latest Version on Packagist](https://img.shields.io/packagist/v/dirk-kokx/Scopes.svg?style=flat-square)](https://packagist.org/packages/dirk-kokx/scopes)
[![GitHub Tests Action Status](https://img.shields.io/github/workflow/status/dirk-kokx/Scopes/run-tests?label=tests)](https://github.com/dirk-kokx/Scopes/actions?query=workflow%3Arun-tests+branch%3Amaster)
[![Total Downloads](https://img.shields.io/packagist/dt/spatie/Scopes.svg?style=flat-square)](https://packagist.org/packages/dirk-kokx/Scopes)


Through this package scopes can be configured for any url addressing the Laravel application. Through this mechanism the application can hold multiple sites and serve the right content based on the given scope.

## Installation

You can install the package via composer:

```bash
composer require dirk-kokx/scopes
```

You can publish the config file with:
```bash
php artisan vendor:publish --provider="DekaWebdesign\Scopes\ScopesServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
return [
    'driver' => 'config',
    'scopes' => [
        [
            'id' => 1,
            'url' => env('APP_URL', 'http://localhost'),
            'name' => 'Main Website',
            'locales' => [
                'nl' => 'nl_NL',
            ],
            'default_locale' => 'nl',
        ],
    ],
];
```

## Usage

When addressing the app using a 'web' route, the locale is automatically set through the Middleware 'SetScope'. If there are multiple locales set it defaults to the default locale, or sets the one which key it finds as the first segment after the base url.

Scopes can be defined in the config or dynamically through the database. Please run 'php artisan migrate' to set the database table.

A Facade (Scopes) is available with the following methods:

```php
    //returns the current Scope object
    Scopes::getCurrent();

    //returns the key of the currently set locale
    Scopes::getLocalePrefix();
```



## Testing

``` bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](.github/CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email info@deka-webdesign.nl instead of using the issue tracker.

## Credits

- [Dirk Kokx](https://github.com/Dirk-Kokx)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
